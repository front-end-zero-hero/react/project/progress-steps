import React, { ReactElement, useState, useRef } from "react"


const circles = [1,2,3,4];

const App: React.FC = (): ReactElement => {
  const [currentStep, setCurrentStep] = useState(1);
  const progressRef = useRef<HTMLDivElement>(null);

  const next = () => {
    setCurrentStep((prev: number) => {
      if(prev === circles.length)
        return prev;
      
      const next = prev + 1;
      handleProgressStyle(next);
      return next;
    })
  }

  const prev = () => {
    setCurrentStep(prev => {
      if(prev === 1)
        return prev;

      const next = prev - 1;
      handleProgressStyle(next);
      return next;
    })
  }

  const handleProgressStyle = (currentActive: number) => {
    const width = ((currentActive - 1) / (circles.length - 1)) * 100;
    if(progressRef && progressRef.current) {
      progressRef.current.style.width = `${width}%`;
    }
  }

  return (
    <>
      <div className="container">
        <div className="progress-container">
        <div className="progress" ref={progressRef}></div>
          {
            circles.map((value: number) => {
              return (
                <div key={value} className={value <= currentStep ? 'circle active' : 'circle'}>{value}</div>
              )
            })
          }
        </div>

        <button className="btn" disabled={currentStep == 1} onClick={prev}>Prev</button>
        <button className="btn" disabled={currentStep == circles.length} onClick={next}>Next</button>
      </div>
    </>
  )
}

export default App
